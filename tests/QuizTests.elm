module QuizTests exposing (allTests)

import Expect exposing (true)
import Fuzz exposing (int)
import Html.Events exposing (..)
import List exposing (length, map, range, sort)
import Model exposing (..)
import Random exposing (initialSeed)
import Set
import Shuffle exposing (shuffle)
import Test exposing (..)
import Test.Html.Event as E
import Test.Html.Query as Q
import Test.Html.Selector as S
import Time exposing (millisToPosix)
import Tuple exposing (first)
import Update
import View


allTests : Test
allTests =
    describe "all tests" [ viewTests, shuffleTests ]


viewTests : Test
viewTests =
    describe "Views"
        [ {- test "Welcome view has an 'Start' button" <|
                 \() ->
                     View.viewWelcome (Update.init () |> first)
                         |> Q.fromHtml
                         |> Q.find [ S.tag "button" ]
                         |> Q.has [ S.text "Start" ]
             , test "OK button returns a CloseWelcomeScreen" <|
                 \() ->
                     View.viewWelcome (Update.init () |> first)
                         |> Q.fromHtml
                         |> Q.find [ S.tag "button" ]
                         |> E.simulate E.click
                         |> E.expect CloseWelcomeScreen
             ,
          -}
          fuzz int "Starting the app gives us a new random seed" <|
            \r1 ->
                let
                    model =
                        Update.init () |> first
                in
                Update.update (StartApp (Time.millisToPosix r1)) model
                    |> first
                    |> .randomSeed
                    |> Expect.notEqual model.randomSeed
        ]


shuffleTests : Test
shuffleTests =
    let
        short =
            List.range 1 4

        long =
            List.range 1 20
    in
    describe "Shuffling a list"
        [ fuzz int "Shuffle can handle an empty list" <|
            \r ->
                shuffle (initialSeed r)
                    []
                    |> Expect.equal []
        , fuzz int "A shuffled list is different from the original list" <|
            \r ->
                let
                    l1 =
                        shuffle (initialSeed r) long
                in
                Expect.notEqual l1 long
        , fuzz int "Shuffling does not change the contents" <|
            \r ->
                let
                    l1 =
                        shuffle (initialSeed r) long
                in
                Expect.equal (sort l1) long
        , fuzz int "Shuffling does not repeat itself frequently" <|
            \r ->
                List.range r (r + 20)
                    |> map (\rr -> shuffle (initialSeed rr) long)
                    |> Set.fromList
                    |> Set.size
                    |> Expect.greaterThan 18
        ]
