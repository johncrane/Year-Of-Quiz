module Model exposing (Category(..), Datum, Difficulty(..), Mode(..), Model, Msg(..))

import Maybe exposing (Maybe)
import Random exposing (Seed)
import State exposing (..)
import Time exposing (Posix, millisToPosix)


type Difficulty
    = Easy
    | Hard


type Category
    = Building
    | FIFA
    | Hurricane
    | Movie
    | Music
    | NoCategory
    | Olympics
    | Politics
    | Science
    | Sports
    | Stage
    | Tv


type alias Datum =
    ( Category, Int, String )


type Mode
    = ShowManyQuestions
    | ShowManyAnswers
    | ChooseMode
    | PlayGame
    | SelfRunning
    | WelcomeScreen


type alias Model =
    { difficulty : Difficulty
    , facts : List Datum
    , isHidden : Bool
    , isSelfRunning : Bool
    , latestTime : Maybe Posix
    , mode : Mode
    , printSeed : Seed
    , randomSeed : Maybe Seed
    , state : State Msg
    , sort : Int
    }


type Msg
    = CloseWelcomeScreen
    | FirstRoll Int
    | PrintAnswers
    | PrintQuiz
    | Next
    | Reveal
    | Start
    | StartApp Posix
    | StartSelfRunning
    | Tick Posix
    | ToggleDifficulty
