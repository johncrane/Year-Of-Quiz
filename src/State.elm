module State exposing (State(..), getMsg, initCountdown, millisRemaining)

import Time exposing (Posix, millisToPosix, posixToMillis)


{-| `State` defines a state of a state machine. The `Countdown` state counts
down time. At the end of the countdown a message is sent. The `DoNothing` state
does nothing and does not msg.
-}
type State msg
    = Countdown Posix Posix msg
    | DoNothing


getMsg : State msg -> Maybe msg
getMsg state =
    case state of
        Countdown start duration message ->
            Just message

        DoNothing ->
            Nothing


{-| To initialize a `Countdown` pass the start time in ms, the duration of the
countdown and the msg to send when completed.
-}
initCountdown : Int -> Int -> msg -> State msg
initCountdown start duration m =
    Countdown (millisToPosix start) (millisToPosix duration) m


{-| Get the amount of time remaining, that is the difference between the
current time passed in and the end of the state. States that don't time out
will return `Nothing`
-}
millisRemaining : State msg -> Posix -> Maybe Int
millisRemaining state curTime =
    case state of
        Countdown start duration msg ->
            Just ((posixToMillis start + posixToMillis duration) - posixToMillis curTime)

        DoNothing ->
            Nothing
