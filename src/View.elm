module View exposing (view, viewWelcome)

import Browser exposing (Document)
import Element as E
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Model exposing (..)
import Shuffle exposing (shuffle)
import State exposing (State, millisRemaining)
import String exposing (fromInt)
import Time exposing (millisToPosix, posixToMillis)
import Update exposing (getSeed, justOrDefault)


black =
    E.rgb 0 0 0


white =
    E.rgb 1 1 1


darkBlue =
    E.rgb 0 0 0.9


lightBlue =
    E.rgb 0.5 0.5 0.9


buttonStyle : List (E.Attribute Msg)
buttonStyle =
    [ Background.color lightBlue
    , Font.color white
    , Border.color darkBlue
    , Border.width 2
    , E.paddingXY 8 8
    , Border.rounded 4
    , E.centerX
    ]


answerStyle : Bool -> List (E.Attribute Msg)
answerStyle highlight =
    [ E.alignLeft
    , Border.color white
    , Border.width 2
    , E.paddingXY 4 4
    ]
        ++ (if highlight then
                [ Font.color white
                , Background.color lightBlue
                , Border.color darkBlue
                , Border.rounded 4
                ]

            else
                [ Font.color black
                , Background.color white
                , Font.size 18
                ]
           )


headerStyle : Int -> List (E.Attribute Msg)
headerStyle level =
    [ Region.heading level
    , E.centerX
    , Font.size (((level - 1) * -8) + 36)
    ]


indexedConcatMap pred list =
    let
        aux index l =
            case l of
                [] ->
                    []

                head :: tail ->
                    pred index head ++ aux (index + 1) tail
    in
    aux 0 list


{-| used for a printable version of the quiz
-}
printDatum : Int -> Bool -> Datum -> List (Html Msg)
printDatum matchYear isHidden ( category, year, event ) =
    [ p []
        [ text
            (if isHidden then
                "----"

             else
                fromInt year
            )
        , text (" " ++ event)
        ]
    ]


view : Model -> Document Msg
view model =
    let
        b =
            case model.mode of
                PlayGame ->
                    viewGame model

                SelfRunning ->
                    viewGame model

                ShowManyAnswers ->
                    -- viewMany model
                    viewWelcome model

                ShowManyQuestions ->
                    --  viewMany model
                    viewWelcome model

                ChooseMode ->
                    viewMode model

                WelcomeScreen ->
                    viewWelcome model
    in
    { title = "What Year Was It?"
    , body = [ b ]
    }


{-| show one item in the quize
-}
viewDatum : Int -> Bool -> Int -> Datum -> List (E.Element Msg)
viewDatum matchYear isHidden index ( category, year, event ) =
    let
        highlight =
            matchYear == year && not isHidden
    in
    [ E.el (answerStyle highlight)
        (E.text
            (if isHidden then
                "----"

             else
                fromInt year
            )
        )
    , E.el (answerStyle highlight)
        (E.text
            (fromInt (index + 1) ++ ": " ++ event)
        )
    ]


viewGame : Model -> Html Msg
viewGame model =
    let
        secs =
            case model.latestTime of
                Nothing ->
                    ""

                Just latestTime ->
                    case millisRemaining model.state latestTime of
                        Nothing ->
                            ""

                        Just ms ->
                            if model.isHidden then
                                String.fromInt (ms // 1000)

                            else
                                ""

        ( c, matchYear, e ) =
            justOrDefault (List.head model.facts) ( NoCategory, 0, "This never should happen!" )
    in
    E.layout
        [ Font.size 20
        ]
    <|
        E.column
            [ E.width (E.px 800)
            , E.height E.shrink
            , E.centerX
            , E.spacing 36
            , E.padding 40
            ]
            [ E.el
                (headerStyle 1)
                (E.text "Which two events happened in the same year?")
            , E.el
                (headerStyle 2)
                (E.text secs)
            , E.column [ E.alignLeft, E.spacing 20 ]
                (indexedConcatMap
                    (viewDatum matchYear model.isHidden)
                    (shuffle (getSeed model) model.facts)
                )
            , E.row [ E.alignRight, E.spacing 20 ]
                [ Input.button
                    buttonStyle
                    { onPress = Just Next
                    , label = E.text "Next"
                    }
                , Input.button
                    buttonStyle
                    { onPress = Just Reveal
                    , label = E.text "Show Years"
                    }
                ]
            ]


{-| used for a printable version of the quiz
-}
viewMany : Model -> Html Msg
viewMany model =
    let
        ( c, matchYear, e ) =
            justOrDefault (List.head model.facts) ( NoCategory, 0, "This never should happen!" )
    in
    div
        [ style "font-size" "10"
        , style "top" "50px"
        , style "left" "20px"
        ]
        ([ button [ onClick PrintQuiz ] [ text "More" ]
         , button [ onClick PrintAnswers ] [ text "Show Years" ]
         , p [] [ text "Which two events happened in the same year?" ]
         ]
            ++ List.concatMap
                (printDatum matchYear model.isHidden)
                model.facts
        )


viewMode : Model -> Html Msg
viewMode model =
    E.layout
        [ Font.size 20
        ]
    <|
        E.column
            [ E.width (E.px 800)
            , E.height E.shrink
            , E.centerX
            , E.spacing 36
            , E.padding 10
            ]
            [ E.el
                (headerStyle 1)
                (E.text "Choose a mode")
            , E.el
                (headerStyle 2)
                (E.text "No Pressure!")
            , Input.button
                buttonStyle
                { onPress = Just Start
                , label = E.text "Play!"
                }
            , E.el
                (headerStyle 2)
                (E.text "Self-running")
            , Input.button
                buttonStyle
                { onPress = Just StartSelfRunning
                , label = E.text "Run!"
                }
            , E.el
                (headerStyle 2)
                (E.text "Generate 10 questions")
            , Input.button
                buttonStyle
                { onPress = Just CloseWelcomeScreen
                , label = E.text "Start!"
                }
            ]


viewWelcome : Model -> Html Msg
viewWelcome model =
    E.layout
        [ Font.size 20
        ]
    <|
        E.column
            [ E.width (E.px 800)
            , E.height E.shrink
            , E.centerX
            , E.spacing 36
            , E.padding 10
            ]
            [ E.el
                (headerStyle 1)
                (E.text "Welcome to the Year of Quiz")
            , E.el
                (headerStyle 2)
                (E.text "A trivia challenge best faced with friends")
            , Input.button
                buttonStyle
                { onPress = Just CloseWelcomeScreen
                , label = E.text "Start!"
                }
            ]
