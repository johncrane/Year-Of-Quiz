module Shuffle exposing (shuffle)

import List exposing (drop, head, length, tail, take)
import Maybe exposing (withDefault)
import Random exposing (Generator, Seed, int)


{-| stole this from Random.elm, don't know why I couldn't import it
-}
bool : Generator Bool
bool =
    Random.map ((==) 1) (int 0 1)


{-| rotate the elements of a list, negative count rotates in the opposite direction
-}
rotate : Int -> List a -> List a
rotate n list =
    case list of
        [] ->
            []

        _ ->
            let
                r =
                    modBy (List.length list) n
            in
            List.drop r list ++ List.take r list


{-| Randomly reorder elements in a list
-}
shuffle : Seed -> List a -> List a
shuffle seed list =
    case list of
        [] ->
            []

        [ y ] ->
            [ y ]

        x :: xs ->
            let
                ( v, seed2 ) =
                    Random.step (int 0 (length xs)) seed

                vMsg =
                    "v = " ++ String.fromInt v ++ " of " ++ String.fromInt (length xs)

                list3 =
                    rotate v (x :: xs)
            in
            withDefault x (head list3) :: shuffle seed2 (withDefault [] (List.tail list3))
